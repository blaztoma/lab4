﻿namespace Lab3.Step1
{
    class Branch
    {

        public string Town { get; set; }
        public Dog[] Dogs { get; set; }
        public Cat[] Cats { get; set; }
        public int DogCount { get; private set; }
        public int CatCount { get; private set; }

        public Branch(string town)
        {
            Town = town;
            Dogs = new Dog[Program.MaxNumberOfAnimals];
            Cats = new Cat[Program.MaxNumberOfAnimals];
        }

        public void AddDog(Dog dog)
        {
            Dogs[DogCount] = dog;
            DogCount++;
        }

        public void AddCat(Cat cat)
        {
            Cats[CatCount] = cat;
            CatCount++;
        }
    }
}
