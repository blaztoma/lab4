﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Lab3.Step1
{
    class Program
    {
        public const int NumberOfBranches = 3;
        public const int MaxNumberOfBreeds = 10;
        public const int MaxNumberOfAnimals = 50;

        static void Main(string[] args)
        {
            Branch[] branches = new Branch[NumberOfBranches];

            branches[0] = new Branch("Kaunas");
            branches[1] = new Branch("Vilnius");
            branches[2] = new Branch("Šiauliai");

            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.csv");

            foreach (string path in filePaths)
            {
                ReadAnimalData(path, branches);
            }

            Console.WriteLine("Kaune užregistruoti šunys:");
            PrintAnimalsToConsole(branches[0].Dogs, branches[0].DogCount);
            Console.WriteLine();

            Console.WriteLine("Kaune užregistruotos katės:");
            PrintAnimalsToConsole(branches[0].Cats, branches[0].CatCount);

            Console.WriteLine();
            Console.WriteLine("Agresyvus Kauno šunys: {0}", CountAggressive(branches[0].Dogs, branches[0].DogCount));
            Console.WriteLine("Agresyvus Vilniaus šunys: {0}", CountAggressive(branches[1].Dogs, branches[1].DogCount));

            Animal[] kaunasDogs = branches[0].Dogs;
            Animal[] vilniusCats = branches[1].Cats;
            int kaunasDogsCount = branches[0].DogCount;
            int vilniusCatsCount = branches[1].CatCount;

            Console.Out.WriteLine("Populiariausia šunų veislė Kaune: {0}", GetMostPopularBreed(kaunasDogs, kaunasDogsCount));
            Console.Out.WriteLine("Populiariausia kačių veislė Vilniuje: {0}", GetMostPopularBreed(vilniusCats, vilniusCatsCount));
            Console.WriteLine();

            Console.WriteLine("Surūšiuotas visų filialų šunų sąrašas:");
            Console.WriteLine();
            Dog[] allDogs = new Dog[Program.MaxNumberOfAnimals * Program.NumberOfBranches];
            int allDogsCount = 0;
            for (int i = 0; i < NumberOfBranches; i++)
            {
                for (int j = 0; j < branches[i].DogCount; j++)
                {
                    allDogs[allDogsCount] = branches[i].Dogs[j];
                    allDogsCount++;
                }
            }
            Animal[] sortedDogs = SortAnimals(allDogs, allDogsCount);
            PrintAnimalsToConsole(sortedDogs, allDogsCount);

            Console.Read();
        }

        static Boolean IsFirstNameLetterLowerCase(string name)
        {
            if (Char.IsLower(name.Substring(0, 1)[0])) return true;
            return false;
        }

        static string CorrectName(string name)
        {
            if (IsFirstNameLetterLowerCase(name)) name = name.Substring(0, 1).ToUpper() + name.Substring(1).ToLower();
            return name;
        }

        public static bool IsPhoneNumber(string number)
        {
            return Regex.Match(number, @"^(\+[0-9]{11})$").Success;
        }

        static void PrintAnimalsToConsole(Animal[] animals, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("Nr {0,-2}: {1}", (i + 1), animals[i].ToString());
            }
        }

        private static Branch GetBranchByTown(Branch[] branches, string town)
        {
            for (int i = 0; i < NumberOfBranches; i++)
            {
                if (branches[i].Town == town)
                {
                    return branches[i];
                }
            }
            return null;
        }

        private static void ReadAnimalData(string file, Branch[] branches)
        {

            string town = null;

            using (StreamReader reader = new StreamReader(@file))
            {
                string line = null;
                line = reader.ReadLine();
                if (line != null)
                {
                    town = line;
                }
                Branch branch = GetBranchByTown(branches, town);
                while (null != (line = reader.ReadLine()))
                {
                    string[] values = line.Split(',');
                    char type = Convert.ToChar(line[0]);
                    string name = values[1];
                    int chipId = int.Parse(values[2]);
                    string breed = values[3];
                    string owner = values[4];
                    string phone = values[5];
                    DateTime vd = DateTime.Parse(values[6]);

                    switch (type)
                    {
                        case 'D':
                            //atkreipkite dėmesį - šunys turi papildomą požymį "aggressive"
                            bool aggressive = bool.Parse(values[7]);
                            Dog dog = new Dog(name, chipId, breed, owner, phone, vd, aggressive);
                            if (!branch.Dogs.Contains(dog))
                            {
                                branch.AddDog(dog);
                            }
                            break;
                        case 'C':
                            Cat cat = new Cat(name, chipId, breed, owner, phone, vd);
                            if (!branch.Cats.Contains(cat))
                            {
                                branch.AddCat(cat);
                            }
                            break;
                    }
                }
            }

        }

        private static void GetBreeds(Animal[] animals, int animalCount, out string[] breeds, out int breedCount)
        {
            breeds = new string[MaxNumberOfBreeds];
            breedCount = 0;

            for (int i = 0; i < animalCount; i++)
            {
                if (!breeds.Contains(animals[i].Breed))
                {
                    breeds[breedCount++] = animals[i].Breed;
                }
            }
        }


        private static void FilterByBreed(Animal[] animals, int animalCount, string breed, out Animal[] filteredAnimals, out int filteredAnimalsCount)
        {
            filteredAnimals = new Animal[MaxNumberOfAnimals];
            filteredAnimalsCount = 0;

            for (int i = 0; i < animalCount; i++)
            {
                if (animals[i].Breed == breed)
                {
                    filteredAnimals[filteredAnimalsCount++] = animals[i];
                }
            }
        }

        private static int CountAggressive(Dog[] dogs, int dogCount)
        {
            int counter = 0;
            for (int i = 0; i < dogCount; i++)
            {
                if (dogs[i].Aggressive)
                {
                    counter++;
                }
            }

            return counter;
        }

        private static string GetMostPopularBreed(Animal[] animals, int animalCount)
        {
            String popular = "not found";
            int count = 0;

            int breedCount = 0;
            string[] breeds;

            GetBreeds(animals, animalCount, out breeds, out breedCount);

            for (int i = 0; i < breedCount; i++)
            {
                Animal[] filteredAnimals;
                int filteredBreedCount = 0;
                FilterByBreed(animals, animalCount, breeds[i], out filteredAnimals, out filteredBreedCount);
                if (filteredBreedCount > count)
                {
                    popular = breeds[i];
                    count = filteredBreedCount;
                }
            }

            return popular;
        }

        //funkcija gražins surikiuotą gyvūnų sąrašą
        private static Animal[] SortAnimals(Animal[] animals, int animalCount)
        {
            for (int i = 0; i < animalCount - 1; i++)
            {
                Animal minValueAnimal = animals[i];
                int minValueIndex = i;
                for (int j = i + 1; j < animalCount; j++)
                {
                    if (animals[j] <= minValueAnimal)
                    {
                        minValueAnimal = animals[j];
                        minValueIndex = j;
                    }
                }
                animals[minValueIndex] = animals[i];
                animals[i] = minValueAnimal;
            }
            return animals;
        }

    }
}
