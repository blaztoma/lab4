﻿using System.Collections.Generic;
using System.Linq;

namespace Lab3.Step3
{
    class Branch
    {
        public Branch(string town)
        {
            Town = town;
            Dogs = new List<Dog>();
            Cats = new List<Cat>();
        }

        public string Town { get; set; }
        public List<Dog> Dogs { get; set; }
        public List<Cat> Cats { get; set; }

        public List<Animal> GetDogsAsAnimals()
        {
            return Dogs.Cast<Animal>().ToList();
        }

        public List<Animal> GetCatsAsAnimals()
        {
            return Cats.Cast<Animal>().ToList();
        }
    }
}
