﻿using System;

namespace Lab3.Step3
{
    class Dog : Animal
    {
        private static int VaccinationDuration = 1;

        public Dog(string name, int chipId, string breed, string owner, string phone, DateTime vaccinatinDate, bool aggressive)
            : base(name, chipId, breed, owner, phone, vaccinatinDate)
        {
            Aggressive = aggressive;
        }

        public bool Aggressive { get; set; }

        //abstraktaus Animal klasės metodo realizacija
        public override bool isVaccinationExpired()
        {
            return VaccinationDate.AddYears(VaccinationDuration).CompareTo(DateTime.Now) > 0;
        }

        public override String ToString()
        {
            return String.Format("Dog - ChipId: {0,5}, Name: {1,10}, Owner: {2,16} ({3}), Last vaccination date: {4:yyyy-MM-dd}", ChipId, Name, Owner, Phone, VaccinationDate);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Dog); //kviečiame tipui specifinį metodą toje pačioje klasėje
        }

        public bool Equals(Dog dog)
        {
            return base.Equals(dog); //kviečiame tėvinės klasės Animal Equals metodą

            //galima papildomai tikrinti pagal tik Dog klasės būdingas sąvybes, pvz
            //return base.Equals(dog) && this.Aggressive == dog.Aggressive;  
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}